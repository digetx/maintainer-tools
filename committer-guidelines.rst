.. _committer-guidelines:

====================
Committer Guidelines
====================

This document gathers together committer guidelines.

.. toctree::
   :maxdepth: 2

   committer-drm-misc
   committer-drm-intel
