.. _contributing:

========================
Contributing and Contact
========================

Please use the `dim-tools@lists.freedesktop.org`_ mailing list for
contributions, questions, and discussion about the tooling and documentation.

Please file `bug reports and feature requests`_ at the `project home page`_.

Please make sure your patches pass the build and self tests by running::

  $ make check

Push the patches once you have an ack from :ref:`maintainers`.

.. _dim-tools@lists.freedesktop.org: https://lists.freedesktop.org/mailman/listinfo/dim-tools

.. _bug reports and feature requests: https://gitlab.freedesktop.org/drm/maintainer-tools/issues

.. _project home page: https://gitlab.freedesktop.org/drm/maintainer-tools/
